#
# Build the application software archive for a DOT Buoy
#
DATE    ?= $(shell date +%FT%T%z)
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || \
			cat $(CURDIR)/.version 2> /dev/null || echo v0)

SUBDIRS = mlf2cmds
ROOTDIR = $(CURDIR)/dist
BINDIR = $(ROOTDIR)/usr/local/bin

PKG = $(CURDIR)/cfsbc-progs-$(VERSION).tar.gz

.PHONY: all
all: $(BINDIR)
	for d in $(SUBDIRS); do \
	     test -d $$d && env GOOS=linux GOARCH=arm GOARM=7 $(MAKE) -C $$d $@ && cp -v $$d/bin/$$d $(BINDIR); \
	done

$(BINDIR) $(ROOTDIR)::
	@mkdir -p $@

services: $(ROOTDIR)
	echo "$(VERSION)" > $(ROOTDIR)/.appversion
	$(MAKE) ROOTDIR=$(ROOTDIR) -C cf-config install

dist: $(PKG)

$(PKG): all services
	tar -C $(ROOTDIR) -c -v -z --owner=0 --group=0 -f $(PKG) .

.PHONY: clean
clean:
	rm -rf dist
